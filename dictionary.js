// requires: sg_dict_manager.js for inDictionary() function

function Dictionary(name, canCapitalize, explanation) {

  this.one = [];          // forms that should be replaced
  this.two = [];          // their replacements
  this.detectables = [];  // same as "one", but sorted, for speedup
  this.sorted = false;
  this.active = true;
  this.ln = 0;
  this.canCapitalize = canCapitalize;
  this.succ = 0;
  this.comment = "słownik " + explanation;
  this.name = name;
}

Dictionary.prototype = {

  Add: function(_in, _out) {

    this.one.push(_in);
    this.two.push(_out);
    this.detectables.push(_in);
  },
  
  AddWithEndings(stem, end1, end2) {
    this.one.push(stem+end1);
    this.two.push(stem+end2);
    this.detectables.push(stem+end1);
  },

  AddSeries(stem, strToSplit) {
     e = strToSplit.replace(" ", ""); // obsłuż różne zapisy
	 additions = strToSplit.split(",");
	 ln = additions.length;
	 for (i = 0; i <ln; i++) {
		 endings = additions[i].split(">");
		 this.AddWithEndings(stem, endings[0],endings[1]);
	 }	 
  },  
  
  expandEnding(stem, endRule) {
  
    switch (endRule) {
        case undefined:
            alert('Błąd w słowniku ' + this.name + ': ' + stem);
        break;

        case "dz":
            this.Add(stem + 'dz' , stem + 'c');
        break;

        case "em":
		    this.AddSeries(stem, "em>ym,emi>ymi,ém>ym,émi>ymi");
        break;

        case "iem":
		    this.AddSeries(stem, "iem>im,iemi>imi,iém>im,iémi>imi")
        break;

        case "ja":
          this.AddSeries(stem,"ja>ia,ją>ią,ję>ię,je>ie,ji>ii,jo>io,jom>iom,jami>iami,jach>iach");
        break;

        case "jum":
          this.AddSeries(stem, "jum>ium,jami>iami,jom>iom,jów>iów,jach>iach,ja>ia");
        break;

        case "ya_ja":
          this.AddSeries(stem,"ya>ja,yi>ji,ye>je,yę>ję,yach>jach,yami>jami,yom>jom,yo>jo,yą>ją");
        break;

        case "ya_ia": 
          this.AddSeries(stem,"ya>ia,yi>ii,ye>ie,yę>ię,yach>iach,yami>iami,yom>iom,yo>io,yą>ią");
        break;
    }
  },
  
  expandStem: function(stemRule, endRule) {
  
    if (stemRule.contains('|')) {
      var stem = stemRule.split('|');
      var pos = stem.length-1;
      this.expandEnding(stem[pos], endRule);
  
      if (stem.length == 2 && stem[pos-1].contains('/')) {
          var variants = stem[pos-1].split('/');
          var varLn = variants.length;
          for (var j = 0; j < varLn; j++) {
             this.expandEnding(variants[j] + stem[pos], endRule);
          }
      } else {
          this.expandEnding(stem[pos-1]+stem[pos], endRule);
      }

      if (pos > 1) {
         if (stem[pos-2].contains('/')
         && stem[pos-1].contains('/')) {
		    var variants1 = stem[pos-1].split('/');
		    var variants2 = stem[pos-2].split('/');
		    var varLn1 = variants1.length;
		    var varLn2 = variants2.length;
		   
		    for (var j = 0; j < varLn1; j++) {
			    this.expandEnding(variants1[j] + stem[pos], endRule);
			    for (var k = 0; k < varLn2; k++) {
				    this.expandEnding(variants2[k] + variants1[j] + stem[pos], endRule);
			    }
		    }

        } else if (stem[pos-2].contains('/')) {

            var variants = stem[pos-2].split('/');
            this.expandEnding(stem[pos-1] + stem[pos], endRule);
            var varLn = variants.length;
            for (var j = 0; j < varLn; j++) {
                this.expandEnding(variants[j] + stem[pos-1] + stem[pos], endRule);
            }	

        } else if (stem[pos-1].contains('/')) {

            var variants = stem[pos-1].split('/');
            var varLn = variants.length;
            for (var j = 0; j < varLn; j++) {
                this.expandEnding(variants[j] + stem[pos], endRule);
                this.expandEnding(stem[pos-2] + variants[j] + stem[pos], endRule);
            }			 
			 
		} else {
            this.expandEnding(stem[pos-2]+stem[pos-1]+stem[pos], endRule);
		}
	  }
			
	} else {
          this.expandEnding(stemRule, endRule);
	}  
  },

  feedArray: function(arr) {
	
    var i = arr.length;
	
    while (i--) {
        if (arr[i].contains('>')) {
            var tokens = arr[i].split(" > ");
            this.Add(tokens[0], tokens[1]);
	    } else if (arr[i].contains('@')) {
            var tokens = arr[i].split(" @ ");
            var stemRule = tokens[0];
            var endRule = tokens[1];
            this.expandStem(stemRule, endRule); 
        }
    }

    this.detectables.sort();
    this.sorted = true;
  },

  getSize: function() {
    return this.one.length;
  },
  
  changeFullWord: function(tuple) {

    if (tuple.inp == tuple.out) {
        tuple.out = this.Replace(tuple.inp);
		
        if (tuple.inp != tuple.out) 
			tuple.comment = this.comment;
    } 
  },
  
  changeWordPart: function(tuple) {

   if (tuple.inp == tuple.out) {
        tuple.out = this.replacePart(tuple.inp);
        if (tuple.inp != tuple.out
        &&  wordIsKnown(tuple.out)) {
           this.succ++;
           tuple.comment = this.comment;
      } else tuple.out = tuple.inp; // revert if change not confirmed by dictionary
    }

  },

  Replace: function(word) {
  
    // Dictionary is not used
  
    if (!this.active) { 
		return word;
	}
 
    // Speedup: check whether this word is replaceable
    // before looking for a replacement

    if (this.sorted) {

      if (!inDictionary(word, this.detectables)) {
         if (!inDictionary(uncapitalizeFirst(word), this.detectables)) 
             return word;
	  }
    }

    this.succ++;

    var i = this.one.length;
    while (i--) {
        if (word == this.one[i]) 
            return this.two[i];
        if (this.canCapitalize) {
            if (isSaneUpperCase(word)) {
                if (word == capitalizeFirst(this.one[i]))
                   return capitalizeFirst(this.two[i]);
            }
        }
    }

    this.succ--;
    return word;
  },
  
replacePart: function(word) {

    // Dictionary is not used

    if (!this.active) {
		return word;
	}

    ln = this.one.length;
   
    while (ln--) {
        
		if (word.contains(this.one[ln]) ) {
          word = word.repAll(this.one[ln], this.two[ln]);
          return word;
        }
		
        if (word.contains(capitalizeFirst(this.one[ln])) ) {
            word = word.repAll(capitalizeFirst(this.one[ln]), capitalizeFirst(this.two[ln]));
            return word;
        }  
   
    }

    return word;
  },
  
  clearStats: function() {
    this.succ = 0;
  }

}