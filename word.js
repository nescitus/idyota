function isEmpty(word) {
    return word === null || word === "" || word === " ";
}

function isLowerCase(word) {
    return (word[0] === word[0].toLowerCase());
}

function isUpperCase(word) {
    return (word[0] === word[0].toUpperCase());
}

String.prototype.isAllCaps = function() {
    return this.valueOf().toUpperCase() === this.valueOf();
};

String.prototype.isAllLower = function() {
    return this.valueOf().toLowerCase() === this.valueOf();
};

// isSaneUpperCase() returns true if a word
// starts  with capital letter and does not
// contain any more capital letters.

function isSaneUpperCase(word) {

    var result = isUpperCase(word);
    if (result) {
         if (word.replace(/[^A-Z]/g, "").length > 1) {
             return false;
         }
    }
    return result;
}

// capitalizeFirst() returns a word
// with the capitalized first letter

function capitalizeFirst(word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
}

// uncapitalizeFirst() returns a word
// without the capitalized first letter

function uncapitalizeFirst(word) {
    return word.charAt(0).toLowerCase() + word.slice(1);
}