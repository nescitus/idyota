function spanWitId(spanStart, idStr, _class, comment, functStr, word, spanEnd) {	
    return spanStart+'<b><span' + idStr
    + htmlAttribute("class", _class)
    + htmlAttribute("title", comment)
    + functStr + '>'
    + word + '</span></b>' + spanEnd; 
}

function logSpan(_inp, _out, comment, _class, spanCount) {
 
    var id = 'no'+spanCount;
    var linkStr = '<a class="hiddenLink" href="#' + id + '">';

    return   _inp + linkStr + ' > <b><span class="' + _class +'">'
           + _out + '</span></b> '+ '</a>' + comment + '<br>';  
  }
  
function autotuneFormat(s1, s2) {
    return '"' + s1 + ' > ' + s2 + '",<br>';
}

function htmlAttribute(name, value) {
	return ' ' + name + '="' + value + '"';
}