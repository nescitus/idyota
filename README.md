BULLET POINTS

- Idyota is a spellchecker that modernises Polish texts published between 1850 and 1936
- it is released under GPL 3.0
- It is written in JavaScript and shows to the user as a web page which can be accessed offline
- It is definately not safe to put it on a server
- I consider it "advanced beta version": it works as intended, it is fast, but it contains lots of unfinished code

OVERVIEW

Idyota (its name being Polish word "idiot" written using the XIXth ortography) is a specialised spellchecker, modernising Polish texts published before 1936, when a great reform of Polish ortography took place. Most of these texts are in the public domain, and Idyota was meant to facilitate publishing them and bringing them to modern standards. You can find several such texts on Wikisources (see https://pl.wikisource.org/wiki/Kategoria:Literatura_polskiego_dwudziestolecia_mi%C4%99dzywojennego), where books are rendered page by page and word by word, with only cosmetic changes.

Idyota can cope with texts from the 2nd half of the XIXth century, excluding poetry, because some of its fixes are known to change the number of syllables in a word, breaking poem's rhyhtm in the process. Going to the 2nd half of the XVIIIth century would be theoretically possible with changed dictionaries. Earlier on Polish spelling has been in a state of flux, so using a rules-based program to modernise texts that old would make no sense.

CONTRIBUTORS AND LICENSING

Idyota is licensed under GNU General Public License 3.0. Developing it, I obtained some valuable consultations regarding linguistic issues and program architecture from Dorota Kowalska.

INTERFACE

Idyota accepts input text in a HTML form window, then publishes it as a HTML. This HTML can be copied as plain text, complete with HTML/XML tags. At present it is not recommended to process text with tags - it may cause some issues with white spaces.

Changes are displayed in bold and in color. Blue and green bold words are corrected by program, clicking on them reverts a change. Red words in bold are words for which Idyota thinks it can make a correction, one that is not necessarily trustworthy. Clicking on them introduces such a correction. Red words without bold are unknown to the program and uncorrected.

Below the processed text, varius statistics are displayed, including detailed information why a change has been made. Clicking on a word on that list brings user to a place where a change has been made.

SAFETY NOTICE

While You can use Idyota freely, as per GNU General Public License, I'd recommend using it offline. If You want to place it on a web page, then SOLVING ALL THE SAFETY ISSUES IS UP TO YOU. Program takes large chunks of text as input, and does not sanitize them. Furthermore, it places parts of that text in alt attributes of HTML tags. Putting it on a web page like that would expose you to code injection attacks. Just don't do that.

On the other hand if You are able to add an input sanitizer, this is the single greatest contribution You can make.

ALGORITHM

Basic algorithm could be devised in the 1970s. Idyota divides text into tokens spanning from one whitespace to the next one. Then it makes another division within tokens, separating a word from punctuation marks (another possible improvement would be to separate HTML/XML tags in the same manner). Then it checks if a word exists in a big dictionary. Here execution paths fork. For some known but rare words, which are popular OCR misreadings, a change is proposed. If a word isn't found in the main dictionary, program attempts to correct it in a number of ways:

- it calls the dictionary of popular errors
- it tries to make a "transformation by rules" (usually pertaining to word's endings) and if the transformed word is found in a dictionary, it suggests such a correction
- it searches for existing words within edit distance (Levenstein distance) of 1, and suggests the most probable among them

PERFORMANCE

On Intel i7 processor Idyota performs spellchecking of three combined full length novels in under 5 seconds. It would be possible to improve it even further, but it is probably unnecessary, because program still needs human supervision. On the other hand, the number of ways to make it slower is amazing. While trying to change  general program structure, please consider the following:
- order of operations is pretty well optimised at this point
- binary search in dictionary is really fast. If there are a couple of operations to try on a given word, it must come first.
- generating all the words with edit distance of 1 and only then checking for their existence in dictionary is several orders of magnitude faster than calculating edit distance between words in any conceivable way 
- there is a trick that can speed up the program by the factor of 100, provided that you work on a set of similar texts: create a dictionary of words that current algorithm is unable to fix, and mark them red without any calculation. Program maintains list of such words on the fly, but the cost of putting them into array is high, so it's only a 10% speedup.
- other than that, simply adding dictionary of proper names would be a great performance boost.

POSSIBLE CONTRIBUTIONS

- input sanitizer, allowing to put Idyota online
- fixing tokenization of HTML/XML tags or entirely new tokenization algorithm
- dictionary of proper names
- converting Idyota into a web browser plugin
- extending range of possible corrections. Edit distance 2 is probably too expensive, but combining "transformation by rules" and edit 1 distance is definately feasible
- program should probably use some context information. Right now it operates on single words (or sometimes on pairs of words that should not be written separately under modern rules.