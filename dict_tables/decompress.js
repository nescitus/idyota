var full_endings = [
    ",NA > ,na,ne",
    ",TA > ,ta,te",
    "-ACH > ach,ami,em,om", 
    "-BY > by,bym,byś,m",
    "A > ,a,ach,ami,e,em,om",
    "A-CH > a,ach,ami,ą,ę,i,o",
    "A-MI > a,ach,ami",
    "ACH > ,ach,ami,em,om,y,ów,owi,u",
    "ACHx > ,ach,ami,i,om,owi,u,ów",
    "ACHy > ,ach,ami,y,ów",
    "ACHz > ach,ami", 
    "AE > a,e,ej,y,ym,ą",
    "AI > a,e,i,y,ą",	
    "AM > am,aś,o,y",
    "AMI > ,a,ach,ami,e,ą,ę,o,om",
    "AMIx > ,a,ach,ami,e,ą,ę,om,y",
    "ANO > am,amy,ano,asz",
    "ARKA > arka,arki,arce,arkę,arką,arko,arek,arkom,arkami,arkach", // dziennikarka, makaroniarka
    "ARZ > arz,arza,arzowi,arzem,arzu,arze,arzy,arzami,arzach,arzom", // dziennikarz, makaroniarz
    "AŁ > ał,ałby,ałbym,ałbyś,ałem,ałaś,ałeś,ało,ałoby,ałyby,ałyśmy, ałaby,ałabym,ałabyś,ałam",
    "AŚ > aś,eś,by,bym,byś,em,oby,y",
    "BY > ,by,bym,byś,m",
    "BYM > ,by,bym,byś,em,oby,y,aś,eś",
    "BYŚ > a,aby,by,bym,byś,em,oby,y,am,aś",
    "cH > ch,e,ego,ej,m,mi",
    "CH > ch,e,ego,ej,m,mi,emu",
    "CI > ci,ciach,ciami,ciom,cią,ć",
	"Ci > ci,cie",
    "CIE > ,cie,cież,my,myż,ą,ę",
    "CIEx > ,cie,cież,my,myż,ą,ąc,e,ecie,emy,esz,ę,że",
    "CIEŻ > ,cie,cież,my,myż,ą,ąc",
    "CIĄ > ci,ciom,cią,ć",
    "CJA > cja,cje,cją,cję,cji,cjo,cjom,cyj,cjach,cjami",
    "CZEK > czek,czka,czkach,czkami,czki,czkiem,czkom,czkowi,czku,czków",
    "CZYNA > czyn,czyna,czynach,czynami,czyną,czynę,czyny,czynów,czynie,czyno,czynom",
    "E-MY > ,cie,cież,my,myż,ą,ę,e,ecie,emy,esz",
    "ECIE > ,e,ecie,emy,esz",
    "EGO > ego,ej,emu,y,ych,ym,ymi,i,ie",
    "EGOx > ego,emu,o,ych",
    "EGOy > ego,ej,i,y,ym,ymi,emu,ych",
    "EJ > ego,ej,emu,i,y,ym,ymi,ych",
    "EJo > ego,ej,emu,ym,ymi,o,ych",
    "EJx > ,ego,ej,emu,ym,ymi,ych",
    "EJą > ego,ej,emu,ych,ymi,ym,y,o,ą",
	"EŃ > eniu,eń",
    "EM > ,a,e,em,om",
    "EMu > ,ach,ami,om,owi,em,ie,y,ów,u",
    "EMU > ego,emu,ych,ymi",
    "EMx > ,ach,ami,om,owi,em,ie,y,ów",
    "ER > er,era,erach,erami,erem,erom,ery,erze,erowi,erów",
	"EŚ > eś,o,yby",
    "I > a,i,o,ą",
    "IA > ia,iach,iami,ią,ię,ii,ij,io,iom", 
    "ID > id,idach,idami,idem,idom,idowi,idu,idy,idzie,idów",
    "IE > ,a,ach,ami,em,ie,o,om",
    "IEM > ,a,ach,ami,i,iem,om,owi,u,ów",
    "IY > i,y,ym,ą,ej",
    "J > a,ach,ami,e,ą,ę,i,j,o,om",
    "KA > ce,ek,ka,kach,kami,ką,kę,ki,ko,kom",
    "KI > ka,ki,ko,ką,kim,kimi",
    "M > m,mi",
    "METR > metr,metrach,metrami,metrem,metrem,metrowi,metru,metry,metrze,metrów",
    "MY > ,my,myż,ą,ąc",
    "MYŻ > ,cie,cież,my,myż,że",
    "NA > na,ne",
    "NEGO > nego,nej,nemu,nym,nymi",
    "NI > na,ne,nej,ni",
    "NIU > ,na,ne,nej,ni,ny,ń,nym,ną,nia,nie,niu",
	"NY > ni,ny,ną",
	"NĄ > niu,ną",
    "OM > a,ach,ami,e,em,om",
    "OMx > a,e,em,om,u",
    "OWI > ach,ami,om,owi,y,ów",
    "OWIEC > owiec,owca,owcach,owcami,owce,owcem,owcom,owcu,owców,owcowi",
    "OWY > owa,owe,owego,owej,owi,owo,owemu,owy,owym,owymi,owych,ową",
    "SKI > skiego,skiemu",
    "STA > sta,stach,stami,stą,stę,sty,stów,sto,stom",
    "TA > ta,te",
    "TOR > tor,tora,torach,torami,torem,torom,tory,torze,torzy,torów,torowi",
    "U > ,em,ie,y,ów,om,u",
    "UJ > uj,ujże,ujemy,ujesz,ujmy,ujmyż,ują,uję,ujcie,uje",	
    "US > us,usa,usach,usami,usom,usowi,usem,usie,usy,usów",
	"UM > om,um,ów",
    "WSZY > ,ą,ć,my,ono,sz,wszy",
    "Y > a,e,ego,ej,i,y,ym,ymi,emu,ych",
    "Y-M > ej,y,ym,ą",
    "YCH > a,e,ego,ej,emu,y,ym,ymi,ych,ą",
    "YM > ,a,e,i,y,ym,ą",
    "ÓW > ,a,ach,ami,ą,ę,y,ów,o,om",
    "Ą > a,e,y,ą",
    "ĄC > ąc,ąca,ące,ącego,ącej,ącemu,ący,ącym,ącymi,ących,ącą",
    "ĄĘ > a,ach,ami,e,ą,ę,i,o,om",
    "Ć > ,ć,ł",
	"CIĄ > cią,ci,ć",
    "Ę > ,a,ach,ami,ą,ę,ie,o,om,y",
    "Ęx > ,a,ach,ami,ą,ę,om,y",
    "Ł > ć,ł,li",
    "ŚCIE > ście,śmy",
    "ŚCIEb > ,ście,śmy,by",
    "ŚCIEx > byśmy,ście",
    "ŚĆ > ści,ść",
    "ŻE > ,cie,cież,my,myż,ą,że",
];

var part_endings = [
    "*ACH > ach,ami",
	"*AĄ > a,ach,ami,ą,ę",
	"*AE > a,ach,ami,e,em,om,owi",
	"*AŚ > am,aś",
    "*Ą > ą,ę",
	"*BY > by,bym,byś",
	"*EJ > ego,ej",
	"*Ej > a,e,ego,ej",
	"*EK > ce,ek",
	"*EM > em,om",
	"*EŚ > am,aś,eś,o,yby",
	"*ESZ > emy,esz",
	"*IE > em,ie",
	"*LIBYŚCIE > libyście,łybyście",
	"*MI > ch,emu,mi",
	"*MY > my,sz,cie", // potential problem
	"*MYŻ > cie,cież,my,myż",	
	"*NIA > nia,nie,niu",
	"*Ń > ny,ń,nym,ną",
	"*OM > om,owi",
	"*Om > a,ach,ami,om,owi",
	"*SZ > sz,wszy",
	"*ŚMY > ście,śmy",
	"*YBY > yby,yśmy",
	"*YCH > ego,emu,ych,ymi",
	"*YMI > ym,ymi",
	"*ECIE > cież, ecie",
];

var short_endings = [
	"*A > a,ach,ami,",
	"*CH > ch,e,ej",
	"*U > u,ów",
	"*Ł > ć,ł",
	"*M > m,sz",
	"*NO > my,no",
	"*O > o,om",
	"*Y > y,ych",
	"*LIBY > liby,liśmy",
	"*YM > i,y,ym,ą,ej",
	"*NA > na,ne",
	"*ÓW > y,ów",
	"*CI > ci,cie",
	"*I > i,o",
	"*E > e,em",
	"*ZE > ze,zy",
];

var fullEndingReplacer = new UnsortedReplacer("fullEndingReplacer");
fullEndingReplacer.feedArray(full_endings);

var partEndingReplacer = new UnsortedReplacer("partEndingReplacer");
partEndingReplacer.feedArray(part_endings);

var shortEndingReplacer = new UnsortedReplacer("shortEndingReplacer");
shortEndingReplacer.feedArray(short_endings);

function Decompressor() {
    //alert("starting decompressor");
}

Decompressor.prototype = {
	  
  test: function() {
	  alert("active");
  },
  
  decompressEnding: function (end) {
	
    if (end.contains("*")) {
	   end = this.decompressStar(end);
	} else {
	   end = this.decompressFullEnding(end);
	}
	
	return end;
  },
  
  decompressStar: function(end) {
	
	end = partEndingReplacer.replaceSeries(end);
	end = shortEndingReplacer.replaceSeries(end);
	return end;
  },
  
  decompressFullEnding: function(end) {
	return fullEndingReplacer.getReplacement(end);
  },

}