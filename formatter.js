function Formatter() {

};

Formatter.prototype = {

  Preprocess: function(txt) {

    // Formatowanie WL-XML

    txt = txt.repAll(" ", " ");     // TODO: find UTF-8 code for the first space 
    txt = txt.repAll(">", "&gt; ");
    txt = txt.repAll("<", " &lt;");
    txt = txt.repAll("\n", " [NEWLINE] ");
    txt = txt.repAll("[NEWLINE] ?", "[NEWLINE] ");
    txt = this.removeDoubleSpaces(txt);
    return txt;
  },
  
    Postprocess: function(txt) {
    
    txt = txt.repAll("&gt; ", "&gt;");
    txt = txt.repAll(" &lt;", "&lt;");

    // opening tags need a space in front of them

    txt = txt.repAll("&lt;slowo_obce&gt;", " &lt;slowo_obce&gt;");
    txt = txt.repAll("&lt;wyroznienie&gt;", " &lt;wyroznienie&gt;");
    txt = txt.repAll("&lt;tytul_dziela&gt;", " &lt;tytul_dziela&gt;");
    txt = txt.repAll("&lt;osoba&gt;", " &lt;osoba&gt;");

    // closing tags need a space after them
	
    txt = txt.repAll("&lt;/slowo_obce&gt;", "&lt;/slowo_obce&gt; ");
    txt = txt.repAll("&lt;/wyroznienie&gt;", "&lt;/wyroznienie&gt; ");
    txt = txt.repAll("&lt;/tytul_dziela&gt;", "&lt;/tytul_dziela&gt; ");
    txt = txt.repAll("&lt;/osoba&gt;", "&lt;/osoba&gt; ");
    txt = txt.repAll("&lt;/motyw&gt;", "&lt;/motyw&gt; ");

    // punctuation marks and spaces

    txt = this.gluePunctuationToWords(txt);

    txt = txt.repAll("---!", "--- !");
    txt = txt.repAll("---?", "--- ?");
		
    // usuwanie nadmiarowych spacji: <pe> <slowo_obce>
    txt = txt.repAll("&lt;pe&gt; &lt;slowo_obce&gt;", "&lt;pe&gt;&lt;slowo_obce&gt;"); 

    // usuwanie nadmiarowych spacji: przypis do słowa obcego
    txt = txt.repAll("&lt;/slowo_obce&gt; &lt;pe&gt;", "&lt;/slowo_obce&gt;&lt;pe&gt;"); 
	
    // usuwanie nadmiarowych spacji: przypis do tytułu
    txt = txt.repAll("&lt;/tytul_dziela&gt; &lt;pe&gt;", "&lt;/tytul_dziela&gt;&lt;pe&gt;");
	
    // usuwanie nadmiarowych spacji: przypis do wyróżnienia
    txt = txt.repAll("&lt;/wyroznienie&gt; &lt;pe&gt;", "&lt;/wyroznienie&gt;&lt;pe&gt;");

    txt = txt.repAll("&lt;/pe&gt;", "&lt;/pe&gt; ");

    txt = txt.repAll(" &lt;/motyw&gt;", "&lt;/motyw&gt;");
	
    txt = txt.repAll("( ", "(");
    txt = txt.repAll(" )", ")");

    txt = this.gluePunctuationToWLfootnotes(txt);
    this.removeDoubleSpaces(txt);
    txt = txt.repAll(" &lt;/motyw", "&lt;/motyw");

    return txt;
  },
  
  plainToWindow: function(txt) {

    txt = txt.repAll("[NEWLINE] ", "\n");
    txt = txt.repAll(" \n", "\n"); // no space before linebreak
    return txt;
  },

  plainToHTML: function(txt, fixLineBreaks) {
	
    if (fixLineBreaks) {
		
       txt = txt.repAll("[NEWLINE] ", " ");
       txt = txt.repAll("  ", " ");
       txt = txt.repAll("&lt;akap", "<br><br>&lt;akap");                     // <akap>, <akap_dialog>  
       txt = txt.repAll("&lt;naglo", "<br><br>&lt;naglo");                   // <naglowek_rozdzial>  
       txt = txt.repAll("&lt;sekcja_swiatlo", "<br><br>&lt;sekcja_swiatlo"); // <sekcja_swiatlo/>
		
	} else {
       txt = txt.repAll("[NEWLINE] ", "<br>");
       txt = txt.repAll(" <br>", "<br>");
	}
	
    return txt;
  },

  gluePunctuationToWords: function(txt) {
    txt = txt.repAll(" , ", ", ");
    txt = txt.repAll(" .", ".");
    txt = txt.repAll(" !", "!");
    txt = txt.repAll(" ?", "?");
    txt = txt.repAll(" )", ")");
    txt = txt.repAll(" :", ":");
    txt = txt.repAll(" ;", ";");
	
	// special case: quotes around tags
	
	txt = txt.repAll('" <', '"<');
	txt = txt.repAll('> "', '>"');

    return txt;
  },

  gluePunctuationToWLfootnotes: function(txt) {
	txt = txt.repAll("&lt;/pe&gt; ,", "&lt;/pe&gt;,"); 
	txt = txt.repAll("&lt;/pe&gt; .", "&lt;/pe&gt;.");
	txt = txt.repAll("&lt;/pe&gt; ?", "&lt;/pe&gt;?");
	txt = txt.repAll("&lt;/pe&gt; !", "&lt;/pe&gt;!");
	txt = txt.repAll("&lt;/pe&gt; :", "&lt;/pe&gt;:");
	txt = txt.repAll("&lt;/pe&gt; ;", "&lt;/pe&gt;;");
	
	txt = txt.repAll("&lt;/pr&gt; ,", "&lt;/pr&gt;,"); 
	txt = txt.repAll("&lt;/pr&gt; .", "&lt;/pr&gt;.");
	txt = txt.repAll("&lt;/pr&gt; ?", "&lt;/pr&gt;?");
	txt = txt.repAll("&lt;/pr&gt; !", "&lt;/pr&gt;!");
	txt = txt.repAll("&lt;/pr&gt; :", "&lt;/pr&gt;:");
	txt = txt.repAll("&lt;/pr&gt; ;", "&lt;/pr&gt;;");
	
	txt = txt.repAll("&lt;/pa&gt; ,", "&lt;/pa&gt;,"); 
	txt = txt.repAll("&lt;/pa&gt; .", "&lt;/pa&gt;.");
	txt = txt.repAll("&lt;/pa&gt; ?", "&lt;/pa&gt;?");
	txt = txt.repAll("&lt;/pa&gt; !", "&lt;/pa&gt;!");
	txt = txt.repAll("&lt;/pa&gt; :", "&lt;/pa&gt;:");
	txt = txt.repAll("&lt;/pa&gt; ;", "&lt;/pa&gt;;");

    return txt;	
  },
  
  removeDoubleSpaces: function(txt) {
    return txt.repAll("  ", " "); // TODO: use regex
  },

};