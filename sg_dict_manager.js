var decompressor = new Decompressor();

function compressDictionary(inp, pre) {

    var out = [];
    var ln = inp.length;
    var pln = pre.length;
    inp.sort();

    for (var i = 0; i < ln; i++) {
        var cur = inp[i];
        if (cur.length <= pln
        ||  cur.contains("|"))
            out.push(cur);
        else {
            if (inDictionary(pre+cur, inp)) {
                // do nothing, unless double prefix
                if (cur.slice(0,pln) == pre ) {
                    var post = cur.slice(pln);
                    if (inDictionary(post, inp))
                       out.push(post);
                }
            } else if (cur.slice(0,pln) == pre ) {
                var post = cur.slice(pln);

                if (inDictionary(post, inp))
                    out.push(pre + "|" + post);
                else 
                    out.push(cur);
            } else {
                out.push(cur);
            }
        }
    }

    out.sort();
    saveDictionary("allwords", out);
}

function compressTwoStems(inp) {
    var out = [];
    var ln = inp.length;
    var block = false;

    inp.sort();	
	
    for (var i = 0; i < ln; i++) {

		if (inp[i].contains(':')
        &&  inp[i+1].contains(':')
	    &&  inp[i].isAllLower()
		&& inp[i+1].isAllLower()
	    &&  i < ln-2) {
			var div1 = inp[i].split(':');
			var div2 = inp[i+1].split(':');
			
			if (div1[0] == div2[0]) {
				block = true;
				out.push(div1[0]+':'+div1[1]+','+div2[1]);
			} else {
				if (!block) out.push(inp[i]);
				block = false;
			}
		} else {
				if (!block) out.push(inp[i]);
				block = false;
		}
	}
	
    out.sort();
    saveDictionary("allwords", out);
}

function sortDict(inp) {
	var out = inp.sort();
	saveDictionary("allwords", out);
}

function compressNullEnding(inp) {

    var out = [];
    var ln = inp.length;
    inp.sort();
    var block = false;
	
	for (var i = 0; i < ln-1; i++) {
		if (inp[i+1].contains(':') 
		&& inp[i+1].isAllLower() ) {
			var div = inp[i+1].split(':');
			if (div[0] == inp[i]) {
				block = true;
				var cur = div[0] + ':,' + div[1];
				out.push(cur);
			} else {
		      if (!block) out.push(inp[i]);
		      block = false;				
			}
			
		} else {
		  if (!block) out.push(inp[i]);
		  block = false;
		}
		
	}
	
	out.push("żżż");
    out.sort();
    saveDictionary("allwords", out);
}

function compressThird(inp, first, second) {
	
	if (!first.contains(":")) {
		alert("First ending should contain : mark");
		return;
	}
	
    var out = [];
    var ln = inp.length;
    inp.sort();
    var block = false;	
	cnt = 0;
		
	for (var i = 0; i < ln; i++) {
		if (inp[i].contains(first)) {
           var div = inp[i].split(':');
		   if (inp[i+1] == div[0]+second) {
			   block = true;
			   out.push(div[0]+first+","+second);
			   cnt++;
		   } else {
			   out.push(inp[i]);
		   }
		} else {
			if (!block) 
				out.push(inp[i]);
			block = false;
		}
	}
	
	alert(cnt);
    out.sort();
    saveDictionary("allwords", out);
}

function eliminateEnding(inp) {
    var out = [];
    var ln = inp.length;
    inp.sort();
    var block = false;	
	
	for (var i = 0; i < ln; i++) {
		if (inp[i].contains(':MYŻ')) {
           var div = inp[i].split(':');
		   if (inp[i+1] == div[0]+'że') {
			   block = true;
			   out.push(div[0]+':MYŻ');
		   } else {
			   out.push(inp[i]);
		   }
		} else {
			if (!block) out.push(inp[i]);
			block = false;
		}
	}
	
    out.sort();
    saveDictionary("allwords", out);
}

function compressTwoEndings(inp, e1, e2) {
	
	cnt = 0;
    var out = [];
    var ln = inp.length;
    var eln = e1.length;
    inp.sort();
    var block = false;
	
   for (var i = 0; i < ln; i++) {
        if (inp[i].contains(":")) {
            block = false;
            out.push(inp[i]);
        } else {
		var root = inp[i].slice(0, inp[i].length-eln);
		if (root + e1 == inp[i]
		&&  root + e2 == inp[i+1]) {
                block = true;
                out.push(root+':'+e1+','+e2);
				cnt++;
            } else {
                if (!block) 
					out.push(inp[i]);
                block = false;
            }
        }
    }

	alert(cnt);
    out.sort();
    saveDictionary("allwords", out);

}

function compressPrefixAtergo(inp, pre) {
	
	// NOTE: no sorting here
	
    var out = [];
    var ln = inp.length;
	var block = false;
	
	for (var i = 0; i < ln; i++) {
		if (inp[i+1] == pre + inp[i]
		&& !inp[i].contains('|')) {
			block = true;
		    out.push (pre + '|' + inp[i]);
		} else {
			if (!block) 
				out.push(inp[i]);
			block = false;
		}
		
	}
	
	// Problem: returns sorted table
	
	saveDictionary("allwords", out);
	
}

function compareDict() {

  alert("porównanie");
  var absentString = '';
  var diff = 0;
    
  var ln = hints.length;
  for (var i = 0; i < ln; i++) {
      var word = hints[i];
      if (!inDictionary(word, all)) {
          absentString += inDictFormat(hints[i]);
          diff++;
	  }
  }
  
  /*
  var ln = _all.length;
  for (var i = 0; i < ln; i++) {
      var word = _all[i];
      if (!inDictionary(word, all)) 
      alert("w drugim słowniku brak " + word);
  }
  */
  
  outputString(absentString);
}


// extractDictionary()  takes  an  array
// of  words with prefixes and endings,
//  and  creates  an  array of normal-looking words.

function extractDictionary(inp, out) {

    var ln = inp.length;

    for (var i = 0; i < ln; i++) {
        var item = inp[i];
        if (item.contains("|")) {
            var div = item.split("|");
			if (div[1].contains(":")) {
                var secDiv = div[1].split(":");
				secDiv[1] = decompressor.decompressEnding(secDiv[1]);
                var altDiv = secDiv[1].split(",");
                var altCnt = altDiv.length;
				while (altCnt--) {
                    out.push(secDiv[0]+altDiv[altCnt]);
                    out.push(div[0]+secDiv[0]+altDiv[altCnt]);
                }
				
			} else {
                    out.push(div[0]+div[1]);
                    out.push(div[1]);
            }
        } else if (item.contains(":")) { 
            var secDiv = item.split(":");
			secDiv[1] = decompressor.decompressEnding(secDiv[1]);
            var altDiv = secDiv[1].split(",");
            var altCnt = altDiv.length;
            while (altCnt--) {
                out.push(secDiv[0] + altDiv[altCnt]);
            }
        } else {
           out.push(item);
        }
    }
}

// function saveDictionary() creates
// and  downloads a file  containing
// a  dictionary  table  in  form of
// a javaScript array;

function saveDictionary(name, arr) {

    var ln = arr.length;
    var outStr = 'var ' + name + ' = [ \n';

    for (var i = 0; i < ln; i++) {
        outStr += '"' + arr[i] + '",\n';
    }

    outStr += '];';
    download("out.txt", outStr);
}

// function inDictionary() checks whether
// a word exists in an array, using binary
// search.

function inDictionary(word, arr) {
   
    var lo  = 0;
    var hi  = arr.length - 1;
    
	if (hi < 1) { 
		return false;
	}
	
    var mid = Math.floor((hi + lo) / 2);
   
    while(arr[mid] != word && lo < hi) {
      
	  if (word < arr[mid]) 
         hi = mid - 1;
      else if (word > arr[mid]) 
         lo = mid + 1;
	 
      mid = Math.floor((hi + lo) / 2);
    }

    if (arr[mid] == word) { 
		return true;
	}
	
    return false;
}

function checkWord(curr) {

    // always accept newline tag

    if (curr == '[NEWLINE]') 
		return true;

    // always accept one-letter words

    if (curr.length == 1) 
		return true;

    if (curr.isAllCaps()) {
        if (inDictionary(curr, roman_num)) 
			return true;
        curr = curr.toLowerCase();
    }

    if (isSaneUpperCase(curr)) {
       if (inDictionary(curr, names_array)) 
		   return true;
       curr = uncapitalizeFirst(curr);
    }

    if (inDictionary(curr, frequent))     
		return true;
    if (inDictionary(curr, missed_array)) 
		return true;

    // TODO: turn on upon finising dictionary compression
    if (inDictionary(curr, all)) 
		return true;

    // foreign language dictionaries, just in case

    if (inDictionary(curr, franc_array)) 
		return true;
    if (inDictionary(curr, ang_array))   
		return true;
    if (inDictionary(curr, niem_array))  
		return true;

    // tags

    if (inDictionary(curr, wl_array)) 
		return true;

    return false;
}
 
function wordIsKnown(word) {

// DEPENDENCIES: array dontSplitByHyphen[] 
// is located in data.js

    if (word.contains('-')) {
       var parts = word.split('-');
       var ln = parts.length;
       while (ln--) {
          if (parts[ln] == '') 
			  return false;
          if (parts[ln] == undefined) 
			  return false;
          if (dontSplitOnHyphen.includes(parts[ln])) 
			  return false;
          if (!checkWord(parts[ln])) 
			  return false;
       }
          return true;
    }

    return checkWord(word);
}

function wordIsHint(curr) {

    // consulting smaller dictionary for a speedup
	
    if (inDictionary(curr, frequent)) 
		return true;
	
	// consulting proper hint dictionary
	
    return inDictionary(curr, hints); 
}
