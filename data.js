// znaki interpunkcyjne (nie podlegają korekcie,
// jeśli znajdują się na początku/na końcu słowa)

const punctuation = [
    ',', '.', '"', "'", '„', "“", '”', '«', "»", "	", 
    '(', ')', '!', '?', '…', "*", ';', ":", "/",
];

const garbage = ["^", "■", "•"];

// słowa, które mogą być błędnie połączone dywizem
// z innymi słowami

var dontSplitOnHyphen = [
"że",
"tam",
"by",
"co",
"bądź",
"tam",
"jakiejś",
"jakiej",
"na",
"niby",
"po",
"pół",
"sam",
"saint",
"indziej",
"xxx > xxx"
];

// jednolementowy pseudosłownik do zamiany "mię" na "mnie"

var mie_array = [
"mię > mnie",
"xxx > xxx"
];

// poprawiane formy dopełniacza liczby mnogiej

var dlm_arr = [
"wzgórzy > wzgórz", 
"wnętrzy > wnętrz", 
"xxx > xxx"
];

// poprawiane formy biernika liczby pojedynczej

blp_array = [
"biednę > biedną",
"tamtę > tamtą",
"jednę > jedną",
"królowę > królową",
"całę > całą",
"moję > moją",
"niejednę > niejedną",
"służącę > służącą",
"xxx > xxx"
];

// poprawianie starych form słownego zapisu liczb

var count_array = [
"siedm > siedem",
"ośm > osiem",
"jedynas > jedenas",
"jedynaśc > jedenaśc",
"xxxxy > xxxxy"
];