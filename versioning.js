function editHeaders(version) {

  var header = getNamedElement("programHeader");
  var footer = getNamedElement("programFooter");
  
  header.innerHTML = '<b>&nbsp; IDYOTA ' 
                   + version 
                   + '</b> symulacja korektora-praktykanta';
				   
  footer.innerHTML = 'Idyota ' 
                   + version 
				   + ' Kod Paweł Kozioł, słowniki Dorota Kowalska';
}
