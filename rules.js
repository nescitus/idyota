var endRuleSet = [
  {inp:'dz',   out:'c',    comment:' -dz',    active:false, family:'[dzRule]', stat:0},
  
  {inp:'jem',  out:'im',   comment:' -jem',   active:false, family:'[emRule]', stat:0},
  {inp:'jemi', out:'imi',  comment:' -jemi',  active:false, family:'[emRule]', stat:0},
  {inp:'kiem', out:'kim',  comment:' -kiem',  active:false, family:'[emRule]', stat:0},
  {inp:'kiemi',out:'kimi', comment:' -kiemi', active:false, family:'[emRule]', stat:0},
  {inp:'giem', out:'gim',  comment:' -giem',  active:false, family:'[emRule]', stat:0},
  {inp:'giemi',out:'gimi', comment:' -giemi', active:false, family:'[emRule]', stat:0},
  {inp:'em',   out:'ym',   comment:' -em',    active:false, family:'[emRule]', stat:0},
  {inp:'emi',  out:'ymi',  comment:' -emi',   active:false, family:'[emRule]', stat:0},
  {inp:'ém',   out:'ym',   comment:' -ém',    active:false, family:'[emRule]', stat:0},  
  {inp:'émi',  out:'ymi',  comment:' -émi',   active:false, family:'[emRule]', stat:0},
  {inp:'iém',   out:'im',  comment:' -iem',   active:false, family:'[emRule]', stat:0},  
  {inp:'iémi',  out:'imi', comment:' -iemi',  active:false, family:'[emRule]', stat:0}, 
  {inp:'iem',   out:'im',  comment:' -iém',   active:false, family:'[emRule]', stat:0},  
  {inp:'iemi',  out:'imi', comment:' -iémi',  active:false, family:'[emRule]', stat:0},   
  
  {inp:'ji',   out:'ii',   comment:' -ji',   active:false, family:'[jota]', stat:0},
  {inp:'ja',   out:'ia',   comment:' -ja',    active:false, family:'[jota]', stat:0},  
  {inp:'jach', out:'iach', comment:' -jach',  active:false, family:'[jota]', stat:0},
  {inp:'ją',   out:'ią',   comment:' -ją',    active:false, family:'[jota]', stat:0},
  {inp:'ję',   out:'ię',   comment:' -ję',    active:false, family:'[jota]', stat:0},
  {inp:'jami', out:'iami', comment:' -jami',  active:false, family:'[jota]', stat:0},
  {inp:'jo',   out:'io',   comment:' -jo',    active:false, family:'[jota]', stat:0}, 
  {inp:'jom',  out:'iom',  comment:' -jom',   active:false, family:'[jota]', stat:0},
  {inp:'je',   out:'ie',   comment:' -je',    active:false, family:'[jota]', stat:0},
  {inp:'jum',  out:'ium',  comment:' -jum',   active:false, family:'[jota]', stat:0},
  {inp:'jów',  out:'iów',  comment:' -jów',   active:false, family:'[jota]', stat:0}, 
  {inp:'ya',   out:'ja',   comment:' -ya>ja', active:false, family:'[jota]', stat:0},
  {inp:'yach', out:'jach', comment:' -yach>jach', active:false, family:'[jota]', stat:0},
  {inp:'yja', out:'ia',    comment:' -yja>ia', active:false, family:'[jota]', stat:0},
  {inp:'yi',  out:'ji',    comment:' -yi>ji', active:false, family:'[jota]', stat:0},
  {inp:'ye',  out:'je',    comment:' -ye>je', active:false, family:'[jota]', stat:0},
  {inp:'yę',  out:'ję',    comment:' -yę>ję', active:false,  family:'[jota]', stat:0},
  {inp:'yą',  out:'ją',    comment:' -yą>ją', active:false,  family:'[jota]', stat:0},
  {inp:'yo',  out:'jo',    comment:' -yo>jo', active:false, family:'[jota]', stat:0},
  {inp:'yom', out:'jom',   comment:' -yom>jom', active:false, family:'[jota]', stat:0},
  {inp:'ya',  out:'ia',    comment:' -ya>ia', active:false, family:'[jota]', stat:0},
  {inp:'yach',out:'iach',  comment:' -yach>iach', active:false, family:'[jota]', stat:0},
  {inp:'yi',  out:'ii',    comment:' -yi>ii', active:false, family:'[jota]', stat:0},
  {inp:'ye',  out:'ie',    comment:' -ye>ie', active:false, family:'[jota]', stat:0},
  {inp:'yę',  out:'ię',    comment:' -yę>ię', active:false, family:'[jota]', stat:0},
  {inp:'yą',  out:'ią',    comment:' -yą>ią', active:false, family:'[jota]', stat:0},
  {inp:'yo',  out:'io',    comment:' -yo>io', active:false, family:'[jota]', stat:0},  
  {inp:'yom',  out:'iom',  comment:' -yom>iom', active:false, family:'[jota]', stat:0},
  {inp:'eji', out:'ei',  comment:' -eji>ei', active:false, family:'[jota]', stat:0},
  {inp:'uji', out:'ui',  comment:' -uji>ui', active:false, family:'[jota]', stat:0},
  {inp:'oji', out:'oi',  comment:' -oji>oi', active:false, family:'[jota]', stat:0},
  
  {inp:'irn', out:'im', comment:' ocr-irn', active:false, family:'[ocr]', stat:0},  
  {inp:'irni', out:'imi', comment:' ocr-irn', active:false, family:'[ocr]', stat:0},	
  {inp:'icli',out:'ich',  comment:' ocr-cli', active:false, family:'[ocr]', stat:0},	
];

var midRuleSet = [
  {inp:'é',   out:'e',  comment:' -é > e', active:false,  family:'[eAcc]', stat:0},
  
  {inp:'onz', out:'ąz', comment:' -onz-', active:false,  family:'[nasal]', stat:0},
  {inp:'ent', out:'ęt', comment:' -ent-', active:false,  family:'[nasal]', stat:0},
  {inp:'ons', out:'ąs', comment:' -ons-', active:false,  family:'[nasal]', stat:0},
  {inp:'omb', out:'ąb', comment:' -omb-', active:false,  family:'[nasal]', stat:0},
  
  {inp:'ęzt', out:'ęst',comment:' -ęst-', active:false,  family:'[dźwięczność]', stat:0},
  
  {inp:'x',   out:'ks', comment:' x', active:false,  family:'[x_do_ks]', stat:0},
  
  {inp:'nn',  out:'n',  comment:' podwójne', active:false,  family:'[podwójne]', stat:0},
  {inp:'ss',  out:'s',  comment:' podwójne', active:false,  family:'[podwójne]', stat:0},
  
	// below rules would work only in presence of allwords dictionary
	// otherwise "osadzę" is changed into "osadzą"
	// propose correction instead of changing

	// {inp:'ą',     out:'ę',      comment:' ąę', active:false,    family:'[ocr]', stat:0},
    // {inp:'ę',     out:'ą',      comment:' ęą', active:false,     family:'[ocr]', stat:0},
];

  // TODO: zasada dla "jakiem" na początku słowa

var startRuleSet = [
    {inp:'jakiem',  out:'jakim', comment:' jakiem', active:false,  family:'[emRule]', stat:0},
];