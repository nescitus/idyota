// requires util.js
// requires formatter.js
// requires dictionary.js
// requires rules.js
// requires data.js for 
// -the list of letters
// -the default list of dictionaries in use

function Spellchecker(name, debug) {

    this.missed = [];
    this.autotune = '';
    this.logStr = '';
    this.levenStr = '';
    this.txtHTML = '';
    this.txtRAW = '';
    this.knownCnt = 0;
    this.failCnt = 0;
    this.fixCnt = 0;
    this.rulesCnt = 0;
    this.guessCnt = 0;
    this.spanCount = 0;
    this.hypoCnt = 0;
    this.emRule = true;
    this.eAccRule = true;
    this.isSeparating = false;
	this.isUndividing = false; 
    this.freqWords = [];
    this.freqScore = [];
    this.fixLineBreaks = false;
    this.isHTMLreturned = false;

    // sadly we need globals here
    // this will be less painful
    // once edit distance corrections
    // will be done by a separate class

    this.editOptions = [];
    this.editScores = [];
    this.levensteined = false;
    this.hasHypothesis = false;
	this.useEditDistance1 = true;
	this.useEditDistance2 = false;

	
	this.editDistance = new EditDistance("aąbcćdeęfghijklłmnoóprsśtuwyzżź");
	this.editCost = new EditCost();
    this.editCost.init();
	// this.editCost.test();
	this.format = new Formatter();

    // Init dictionaries
    // variable order:
    // 1) dictionary name
    // 2) do we check for capitalization variants?
    // 3) explanation
  
    this.connectDict = new Dictionary("connectDict", true, " (łączenie)");
    this.connectDict.feedArray(connect_array);
  
    this.doubtDict = new Dictionary("doubtDict", true, " (wątpliwe)");
    this.doubtDict.feedArray(doubt_array);
  
    // tables of dictionaries
  
    this.dictSet = [];
    this.dictTrigger = [];
    this.dictUsesFullWords = [];
    this.dictNo = 0;
  
    this.addDictionary("ocrDict", true, "(ocr)", ocr_array, false, "ocr_box");
    this.addDictionary("consDict", true, "(dźwięczność)", cons_array, false, "dzw_box");
    this.addDictionary("divDict", true, "(podział)", divide_array, true, "div_box");
    this.addDictionary("emDict", true, "(-em/-emi)", emEndings, true, "em_box"); 
    this.addDictionary("jotaDict", true, "(jota)", jota_array, true, "j_box"); 
    this.addDictionary("jotaPartDict", true, "(jota-fragment)", partial_jota_array, false, "j_box"); 
  
    this.addDictionary("mieDict", true, "(mię)", mie_array, true, "nas_box");
    this.addDictionary("blpDict", true, "(biernik na ę)", blp_array, true, "nas_box");
    this.addDictionary("dlmDict", true, "(dopełniacz lm)",dlm_arr, true, "gen_box");
    this.addDictionary("countDict", true, "(liczby)", count_array, false, "num_box");
    this.addDictionary("typosDict", true, "(literówki)", typos_array, true, "typo_box");
	
	this.midRulesLn = midRuleSet.length;
    this.endRulesLn = endRuleSet.length;
}

Spellchecker.prototype = {

  addDictionary: function(name, flag, caption, arr, full, trigger) {
    this.dictSet[this.dictNo] = new Dictionary(name, flag, caption);  
    this.dictSet[this.dictNo].feedArray(arr);
    this.dictUsesFullWords[this.dictNo] = full;
    this.dictTrigger.push(trigger);
    this.dictNo++;	
  },
  
  setDictParams: function (num, arr, full, trigger) {

    this.dictSet[num].feedArray(arr);
    this.dictUsesFullWords[num] = full;
    this.dictTrigger.push(trigger);
    this.dictNo++;
  },
  
  getFreq: function (word) {
  
    const ln = this.freqWords.length;
  
    for (var i = 0; i < ln; i++) {
         if (word == this.freqWords[i]) 
             return this.freqScore[i];
    }
 
    return 0;
  },

  addFreq: function (word) {
  
    if (isEmpty(word)) 
		return;
	
    if (!wordIsHint(word))
		return;
     
    var ln = this.freqWords.length;
    word = word.toLowerCase();

	// change frequency of a word already in a dictionary
	
    for (i = 0; i < ln; i++) {
        if (this.freqWords[i] == word) {
            this.freqScore[i]++;
            return;
        }
    }

	// add new word
	
    this.freqWords.push(word);
    this.freqScore.push(1);
  },
    
  buildFreqDict: function(txt) {
 
    var tokens = txt.split(" ");
    var ln = tokens.length;

    // fill dictionary with frequencies

    for (var i = 0; i < ln; i++) {
		
		/*if (i % 100000 == 0) {
			alert("Processed " + i + "tokens of " + ln);
		}*/

        if (tokens[i].length > 2) {
				
            var sp = {prefix:'', curr:tokens[i], postfix:''};
            this.prefixPostfix(sp);

            if (sp.curr.length > 2) 
               this.addFreq(sp.curr);
        } 
    }
  },
  
  getFreqDict: function() {
  
    this.findHTMLentities();
    var txt = this.input.value;
    txt = this.format.Preprocess(txt);
    this.buildFreqDict(txt);
    var ln = this.freqWords.length;

    var dictArray = [];

    for (var i = 0; i < ln; i++) {
        locStr = '"'+this.freqWords[i] + ' ' + this.freqScore[i]+'",\n';
        dictArray.push(locStr);
    }

    dictArray.sort();

    var outStr = '';

    for (i = 0; i < ln; i++) {
        outStr += dictArray[i];
	}

    download("freq.txt", outStr);
  },

  onNewCorrection: function() {

    this.missed = [];
    this.autotune = '';
    this.logStr = '';
    this.levenStr = '';
    this.txtHTML = '';
    this.txtRAW = '';
    this.knownCnt = 0;
    this.failCnt = 0;
    this.fixCnt = 0;
    this.hypoCnt = 0;
    this.rulesCnt = 0;
	this.separationCnt = 0;
    this.guessCnt = 0;
    this.spanCount = 0;

    // clear rules statistics

    for (i = 0; i < this.endRulesLn; i++)
        endRuleSet[i].stat = 0;

    for (i = 0; i < this.midRulesLn; i++)
        midRuleSet[i].stat = 0;
  
    // clear dictionaries statistics

    for (i = 0; i < this.dictNo; i++) {
        this.dictSet[i].clearStats();
	}
	
    this.connectDict.clearStats();
    this.doubtDict.clearStats();
  },
  
  findHTMLentities: function() {

    this.input = getNamedElement("input");
    this.statOutput = getNamedElement("stat");
    this.timeInfo = getNamedElement("time");
    this.suggestOutput  = getNamedElement("suggest");
    this.unknownsOutput  = getNamedElement("unknowns");
    this.levenOutput = getNamedElement("leven");
    this.detailsOutput = getNamedElement("details");
    this.plainOutput = getNamedElement("plainOutput");
    this.htmlOutput  = getNamedElement("htmlOutput");
  },
    
  wordSpan: function(word, comment, _class) {

    this.spanCount++;
    var id = 'no' + this.spanCount;
    var idStr = ' id="' + id + '"';
    var internalStr = "'" + id + "'";
    var functStr = ' onclick="changeWording('+ internalStr +')"'
				 
    var spanStart = " ";
    var spanEnd = " ";			 

    if (this.isHTMLreturned) {	
        spanStart = " &lt;span class='corrected'&gt;";
        spanEnd = "&lt;/span&gt; ";
	
        if (_class == "error") {
            spanStart = " &lt;span class='error'&gt;";
        }
    }

	return spanWitId(spanStart, idStr, _class, comment, functStr, word, spanEnd);
  },
  
  processConnectedWord: function(prefix, word, postfix, oldForm) {

    var newForm = prefix + word + postfix;
    this.knownCnt++;
    this.txtHTML += (this.wordSpan(newForm, oldForm, 'dict') + " ");
    this.txtRAW = this.txtRAW + newForm + " ";
   
    // TODO: real link to the place where the words have been connected
    
	var tuple = {inp:oldForm, 
	             out:newForm, 
				 pre:prefix, 
				 post:postfix, 
				 comment:"(łączenie)"};
				 
    this.appendToLog(tuple, 'połączenie');
  },

  processKnownWord: function(tuple) {

    oldForm = tuple.pre + tuple.inp + tuple.post;
    this.knownCnt++;

    this.doubtDict.changeFullWord(tuple);
	
    if (this.isFixed(tuple)) {
		this.appendToSolution(tuple,'doubt');
	} else {
        this.txtHTML += (oldForm + " ");
    }
    
	this.txtRAW = this.txtRAW + oldForm + " ";
  },
  
  processMidRule: function(tuple, rule) {
  
    if (this.isNotFixed(tuple)) {

        if (tuple.out.contains(rule.inp) ) {
            var fix = tuple.out.repAll(rule.inp, rule.out);
            if (wordIsKnown(fix)) {
               tuple.out = fix;
               tuple.comment = "reguła: " + rule.comment;
               rule.stat++;
               this.rulesCnt++;
            } 
        }  
    }  
  },
  
  processEndRule: function(tuple, rule) {
  
    if (this.isNotFixed(tuple)) {

        var ln = tuple.inp.length;
        var lnOld = rule.inp.length;

        if (tuple.inp.slice(-lnOld) == rule.inp) {
            var root = tuple.inp.slice(0,ln-lnOld);
            var fix = root + rule.out;

            if (wordIsKnown(fix)) {
                tuple.out = fix;
                tuple.comment = "reguła: " + rule.comment;
                this.rulesCnt++;
                rule.stat++;
            }
        }
    }
  },

  processUnknownWord: function(tuple) {

    levensteined = false;
    hasHypothesis = false;
    
    // if our word is not on dynamically updated list
    // of failed corrections, try to fix it 
    
    if (!this.missed.includes(tuple.inp)) {

	    // Step 1. Correction by dictionary:
		// either word-for-word replacement
		// or changing word's stem.
	
        this.fixByDictionary(tuple); // TODO: class dictionaryFixer
		
		// Step 2. Correction by rules:
		// if current word is not in a dictionary,
		// but its transformation according to rules is,
		// then we accept the new form.

		if (this.isNotFixed(tuple))
           this.fixByRules(tuple);

	    // Step 3. Separating "nie"
		
		if (this.isNotFixed(tuple) && this.isSeparating)
			this. fixBySeparation(tuple);

	    // Step 4. Removing "-"
		
		if (this.isNotFixed(tuple) && this.isUndividing) {
			this. fixByUndividing(tuple);
		}
	
        // Step 5. Correction by edit distance 1
    
        if (this.isNotFixed(tuple)
		&&  this.useEditDistance1
        &&  tuple.inp.length > 2) {		   				
			this.fixByEditDistance(tuple);
        }
		
		// Step 6. Correction by edit distance 2 (disabled)
		
		if (this.isNotFixed(tuple)
		&&  this.useEditDistance2
        &&  tuple.inp.length > 4) {		   				
			this.fixByEditDistance2(tuple);
        }
	}

    // Step 7. recording results
  
    if (this.isFixed(tuple)) {
        
       this.displayFixed(tuple, levensteined, hasHypothesis);
    
    } else {  // we don't know a word and it hasn't been fixed 
        this.appendMissedDictionary(tuple.inp);
        this.txtHTML += (spanUnknown(this.getOldForm(tuple)) + " ");
        this.failCnt++;
    }

    this.txtRAW = this.txtRAW + this.getNewForm(tuple) + " ";
  },
  
  fixByDictionary: function(tuple) {
	        for (i = 0; i < this.dictNo; i++) {
            if (this.dictUsesFullWords[i] == true)
                this.dictSet[i].changeFullWord(tuple);
            else
                this.dictSet[i].changeWordPart(tuple);
        }  
  },
  
  fixByRules: function(tuple) {
        
		for (l = 0; l < this.endRulesLn; l++) {
			if (endRuleSet[l].active)
               this.processEndRule(tuple, endRuleSet[l]);
		}
        
		for (l = 0; l < this.midRulesLn; l++) {
			if (midRuleSet[l].active)
                this.processMidRule(tuple, midRuleSet[l]);
		}
  },
  
  fixBySeparation: function(tuple) {
	 
    if (tuple.inp.length < 7) {
	    return;
	}		
	 
	const prefix = tuple.inp.slice(0,3);  
	
	if (prefix == "nie" || prefix == "Nie") {
		var rest = tuple.inp.slice(3);
		
		if (wordIsHint(rest)) {
			tuple.out = prefix + " " + rest;
			tuple.comment = "separation";
			this.separationCnt++;
		}
	}
	
  },
  
  fixGarbageSigns: function(tuple, g) { // TODO: Put in a loop

	 if (tuple.inp.contains(g)) {
        result = tuple.inp.replace(g,"");
		if (wordIsHint(result)) {
			tuple.out = result;
			tuple.comment = "śmieci";
		}
	 }	 
	 
  },
  
  fixByUndividing: function(tuple) {
	  
	 if (tuple.inp.contains("-")) {
        result = tuple.inp.replace("-","");
		if (wordIsHint(result)) {
			tuple.out = result;
			tuple.comment = "dywiz";
		}
	 }	 
  },

  fixByEditDistance: function(tuple) {
	    
	  var isUpper = isSaneUpperCase(tuple.inp);
		
      if (isUpper) {
		  this.uncapitalizeTuple(tuple);
	  }
		
      if (tuple.inp.length > 2) {
			
      var edit = this.editDistance.edit1(tuple.inp);
      var editLn = edit.length;
            			
      this.clearEditDistanceData();
      var levenstein1Success = false;
			
      for (var k = 0; k < editLn; k++) {
          if (wordIsHint(edit[k])) {
			  if (! levenstein1Success)
				  this.guessCnt++;
			  levenstein1Success = true;
              this.editOptions.push(edit[k]);
              this.editScores.push(this.scoreSuggestion(tuple.inp, edit[k]));
              levensteined = true;
              tuple.out = edit[k];
              tuple.comment = ' (dystans edycyjny 1)';
          }
      }
			
      var bestWord = "";

      if (this.editOptions.length > 0) {
          bestWord = this.pickBestEditDistance(tuple);
      }
			
      if (this.editOptions.length == 1)  {
          this.pickOnlyEditDistance(tuple);
		  levensteined = true;
          hasHypothesis = true;
      }
			
      if (this.editOptions.length > 1
      &&  wordIsHint(bestWord))  {
          tuple.out = bestWord;
          this.hypoCnt++;
          tuple.comment = ' (frekwencja 1)';
          levensteined = true;
          hasHypothesis = true;
      }
    }

      if (isUpper) {
		  this.capitalizeTuple(tuple);
	  }
	
  },
  
    fixByEditDistance2: function(tuple) {
	    
	  var isUpper = isSaneUpperCase(tuple.inp);
		
      if (isUpper) {
		  this.uncapitalizeTuple(tuple);
	  }
		
      if (tuple.inp.length > 2) {
			            			
      this.clearEditDistanceData();	
			
	  if (this.isNotFixed(tuple)) {
	  
          edit = this.editDistance.edit2(tuple.inp);
          editLn = edit.length;
          this.editOptions.length = 0;
          this.editScores.length = 0;
			
          for (k = 0; k < editLn; k++) {
              if (wordIsHint(edit[k])) {		  
                  this.editOptions.push(edit[k]);
                  this.editScores.push(this.scoreSuggestion(tuple.inp, edit[k]));
                  levensteined = true;
                  tuple.out = edit[k];
                  tuple.comment = ' (dystans edycyjny 2)';
              }
          }
      }
			
      var bestWord = "";

      if (this.editOptions.length > 0) {
          bestWord = this.pickBestEditDistance(tuple);
      }
			
      if (this.editOptions.length == 1
      &&  !isUpperCase(tuple.inp))  {
          this.pickOnlyEditDistance(tuple);
		  levensteined = true;
          hasHypothesis = true;
      }
    }

      if (isUpper) {
		  this.capitalizeTuple(tuple);
	  }
	
  },

  uncapitalizeTuple: function(tuple) {
      tuple.inp = uncapitalizeFirst(tuple.inp);
      tuple.out = uncapitalizeFirst(tuple.out);	  
  },
  
  capitalizeTuple: function(tuple) {
      tuple.inp = capitalizeFirst(tuple.inp);
      tuple.out = capitalizeFirst(tuple.out);	  
  },
  
  clearEditDistanceData: function() {
      this.editOptions.length = 0;
      this.editScores.length = 0;
	  //https://stackoverflow.com/questions/1232040/how-do-i-empty-an-array-in-javascript
  },
  
  pickBestEditDistance: function(tuple) {
	  
	  var bestScore = 0;
      var bestWord = '';
      var ln = this.editOptions.length;
      this.levenStr += wrapInBold(tuple.inp);

      for (i = 0; i < ln; i++) {

          var currScore = this.editScores[i] + 100 * this.getFreq(this.editOptions[i]);
          if (currScore > bestScore) {
              bestScore = currScore;
              bestWord = this.editOptions[i];
          }

          this.levenStr += wrapInBrackets(this.editOptions[i]);
     }

     this.levenStr += wrapInBold(bestWord) + '<br>';  
     return bestWord;
  },
  
  pickOnlyEditDistance: function(tuple) {
      tuple.out = this.editOptions[0];
      tuple.comment = ' (dystans edycyjny bez alternatyw)';  
	  this.autotune += autotuneFormat(tuple.inp, tuple.out);
	  this.hypoCnt++;
  },
  
  displayFixed: function(tuple, levensteined, hasHypothesis) {
	  
	    var oldForm = this.getOldForm(tuple); 
		var newForm = this.getNewForm(tuple); 
	  
	    if (tuple.comment.contains('reguła')) {
            this.appendToSolution(tuple,'rule');
        } else if (levensteined) {
            if (hasHypothesis) {
                this.appendToSolution(tuple,'error');
            } else {
                this.appendToSolution(tuple,'auto');
            }
        } else {
            this.appendToSolution(tuple, 'dict');
        }
  },

  appendMissedDictionary: function(word) {
	   if (!this.missed.includes(word))
           this.missed.push(word);
  },
  
  isFixed: function(tuple) {
	return tuple.inp != tuple.out;  
  },
  
  isNotFixed: function(tuple) {
	return tuple.inp == tuple.out;  
  },

  getOldForm: function(tuple) {
	  return tuple.pre + tuple.inp + tuple.post;
  },

  getNewForm: function(tuple) {
	  return tuple.pre + tuple.out + tuple.post;
  },
  
  appendToSolution: function(tuple, cathegory) {
	  this.appendToHTML(tuple, cathegory);
	  this.appendToLog(tuple, cathegory);
  },
  
  appendToHTML: function(tuple, cathegory) {
	  
	  this.txtHTML += tuple.pre; // punctuation before word, like a bracket
	  
	  if (this.isSafeCorrection(tuple.comment) )
		  this.txtHTML += this.wordSpan(tuple.out, tuple.inp, cathegory);
	  else
	      this.txtHTML += this.wordSpan(tuple.inp, tuple.out, cathegory);
	  
	  this.txtHTML += tuple.post; // punctuation after word
	  
	  this.txtHTML += " ";
  },
  
  appendToLog: function(tuple, classification) {
	  if (tuple.comment != '')
	      this.logStr += logSpan(tuple.inp, tuple.out, tuple.comment, classification, this.spanCount);
  },
  
  isSafeCorrection(comment) {
	  return comment.contains("reguła") 
	      || comment.contains("słownik") 
		  || comment.contains("separation")
		  || comment.contains("dywiz")
		  || comment.contains("śmieci");
  },
  
  countCorrections: function() {
	  
    this.fixCnt = this.connectDict.succ
                + this.guessCnt
                + this.rulesCnt
				+ this.separationCnt
                + this.doubtDict.succ; // not sure if it should be added

    for (i = 0; i < this.dictNo; i++) {
        this.fixCnt += this.dictSet[i].succ;
    }
  },
  
  printStat: function() {
  
    this.countCorrections();

    var wordCnt = Math.max(this.knownCnt + this.failCnt + this.fixCnt, 1);
    var knownRatio  = getProportion(this.knownCnt, wordCnt);
    var failedRatio = getProportion(this.failCnt, wordCnt);
    var fixedRatio  = getProportion(this.fixCnt, wordCnt);
    var efficiency = getProportion( fixedRatio, fixedRatio + failedRatio);

    var statStr = 
	  'Słów: ' + wordCnt + " "
    + 'Znanych: '      + this.knownCnt  + wrapInBrackets(knownRatio) + " "
    + 'Nieznanych: '   + this.failCnt + wrapInBrackets(failedRatio) + " "
    + 'Poprawek: '     + this.fixCnt  + wrapInBrackets(fixedRatio) + " "
    + 'Skuteczność: '  + efficiency;

    var detailStr = '';
	
    if (this.fixCnt) {
        detailStr = statStr + 'W tym: <br>'
            + 'połączone: '     + this.connectDict.succ + '<br>'
            + 'reguły: '        + this.rulesCnt + '<br>'
            + 'odgadnięte: '    + this.guessCnt + '<br>'
            + 'wątpliwości: '   + this.doubtDict.succ + '<br>'
            + 'hipotezy: '      + this.hypoCnt + '<br>'
            ;
 
            for (i = 0; i < this.dictNo; i++) {
                detailStr += 'Słownik ' + this.dictSet[i].comment + ': ' 
                          + this.dictSet[i].succ + '<br>';
        }
    }

    for (i = 0; i < this.endRulesLn; i++)
        detailStr += endRuleSet[i].comment + ' ' + endRuleSet[i].stat + '<br>';

    for (i = 0; l < this.midRulesLn; i++)
        detailStr += midRuleSet[i].comment + ' ' + midRuleSet[i].stat + '<br>';

    this.statOutput.innerHTML = statStr;  
    this.detailsOutput.innerHTML = detailStr; 

  },
  
  prefixPostfix: function(sp) {

    // split prefix

    var first = sp.curr.slice(0,1);
    while (isPunctuation(first) && sp.curr.length > 1) {
        sp.prefix = sp.prefix + first;
        sp.curr = sp.curr.slice(1);
        first = sp.curr.slice(0,1);
    }

    // split postfix

    var last = sp.curr.slice(-1);
    while (isPunctuation(last) && sp.curr.length > 1) {
        sp.postfix = last+sp.postfix;
        sp.curr = sp.curr.slice(0,sp.curr.length-1);
        last = sp.curr.slice(-1);
    }
  },

  Fix: function(txt) {

    this.logStr = ' ';
    txt = this.format.Preprocess(txt);

    // tokenize

    var tokens = txt.split(" ");
    var ln = tokens.length;

    // Create frequency dictionary of the current text
    // in order to have hints a bit more in line with 
    // the current text. This might take some time.

    var boxId = getNamedElement('freq_box');
    if (boxId.checked) {
       this.buildFreqDict(txt);
	}

    this.initOptionsFromWebpage();			

    // rebuild text

    for (var i = 0; i < ln; i++) {

        // try connecting words

        var hasBeenConnected = false;
        var oldForm = '';

        if (i < ln-1) {
            var sp = {prefix:'', curr:tokens[i] + ' ' + tokens[i+1], postfix:''};
            this.prefixPostfix(sp);

            var tuple = this.createFreshTuple(sp.prefix, sp.curr, sp.postfix);
						 
            this.connectDict.changeFullWord(tuple);
            if (this.isFixed(tuple)) {
                tokens[i] = this.getNewForm(tuple);
                tokens[i+1] = '';
                hasBeenConnected = true;
                oldForm = tuple.inp;
            }
        }

        // safeguard against leading space

        if (tokens[i] == '') 
			continue;

        // discover WL-XML tags; cut off leading 
        // and trailing punctuaton marks, henceforth
        // referred to as sp.prefix and sp.postfix

        var isTag = inDictionary(tokens[i], wl_array);        
		var sp = {prefix:'', curr:tokens[i], postfix:''};
		
        if (!isTag) 
            this.prefixPostfix(sp);
		
		var tuple = this.createFreshTuple(sp.prefix, sp.curr, sp.postfix); 

        // correct the word and display information
        // about what we have done

        if (hasBeenConnected) {
           this.processConnectedWord(sp.prefix, sp.curr, sp.postfix, oldForm);
        } else {			
           if ( this.fixLineBreaks || wordIsKnown(sp.curr) 
           || !isNaN(parseFloat(sp.curr))) 
              this.processKnownWord(tuple);
           else 
              this.processUnknownWord(tuple);
        }
    }

  },
  
  initOptionsFromWebpage: function() {
    this.setActiveDictionaries();
	this.setMidRules(); 
	this.setLooseStuff();
	this.setEndRules();	
  },
  
  setActiveDictionaries: function() {
    for (i = 0; i < this.dictNo; i++)
        this.dictSet[i].active = this.isBoxChecked(this.dictTrigger[i]);
  },

  setLooseStuff: function() {
	this.isHTMLreturned = this.isBoxChecked("html_box");
	this.fixLineBreaks = this.isBoxChecked("line_box");
	this.eAccRule = this.isBoxChecked("e_box");
	this.emRule = this.isBoxChecked("em_box");
	this.connectDict.active = this.isBoxChecked("connect_box");	
	this.isSeparating = this.isBoxChecked("nie_box");
	this.isUndividing = this.isBoxChecked("dyw_box");
	this.useEditDistance1 = this.isBoxChecked("edit1_box"); 	
	this.useEditDistance2 = this.isBoxChecked("edit2_box"); 
  },
  
    setMidRules: function() {
	  
    for (i = 0; i < this.midRulesLn; i++) {
	    this.setSingleMidRule(i, "eAcc", "e_box");
		this.setSingleMidRule(i, "nasal", "nas_box");
		this.setSingleMidRule(i, "dźwięczność", "dzw_box");	
		this.setSingleMidRule(i, "x_do_ks", "x_box");
		this.setSingleMidRule(i, "podwójne", "dbl_box");	
	} 
  },
  
  setEndRules: function() {
	  
	for (i = 0; i < this.endRulesLn; i++) {
	    this.setSingleEndRule(i, "emRule", "em_box");		
		this.setSingleEndRule(i, "jota", "j_box");	
		this.setSingleEndRule(i, "dzRule", "dzw_box");	
        this.setSingleEndRule(i,"ocr", "ocr_box");	
    }  
  },
  
  setSingleMidRule: function(i, familyName, boxName) {
      if (midRuleSet[i].family.contains(familyName))
          midRuleSet[i].active = this.isBoxChecked(boxName);	 
  },
  
  setSingleEndRule: function(i, familyName, boxName) {
      if (endRuleSet[i].family.contains(familyName))
          endRuleSet[i].active = this.isBoxChecked(boxName);	 
  },
    
  isBoxChecked: function(boxName) {
	  boxId = getNamedElement(boxName);
	  return boxId.checked;  	  
  },
  
  createFreshTuple: function(prefix, word, postfix) {
      return {inp:word, out:word, pre:prefix, post:postfix, comment:""};
  },

  makeCorrection: function() {

    var startTime = this.setStartTime();
    this.onNewCorrection();
    this.findHTMLentities();
    var txt = this.input.value;

    this.Fix(txt);
    this.printMissedWords();

    this.plainOutput.innerHTML = this.format.plainToWindow(this.txtRAW);

    this.txtHTML = this.format.plainToHTML(this.txtHTML, this.fixLineBreaks);
    this.txtHTML = this.format.Postprocess(this.txtHTML);
    this.htmlOutput.innerHTML = this.txtHTML;

    this.printStat();
    this.suggestOutput.innerHTML = this.logStr;
    this.levenOutput.innerHTML = this.levenStr;

    this.showElapsedTime(startTime);
  },

  setStartTime: function() {
      var d = new Date();
      return d.getTime();
  },

  showElapsedTime: function(startTime) {
    var e = new Date();
    var runtime = e.getTime() - startTime;
    this.timeInfo.innerHTML = "<br> Czas korekty: " + runtime + " milisekund";
  },

  printMissedWords: function() {

      this.missed.sort();
      var missedLn = this.missed.length;
      var missedWords = '';

      for (var i = 0; i < missedLn; i++) {
          missedWords += inDictFormat(this.missed[i]);
      }

      this.unknownsOutput.innerHTML = missedWords;
  },

  scoreSuggestion: function(w1, w2) {

    var result = 0;
    var ln = 0;
	
	result = 500 - this.editCost.calculate(w1, w2);
	
	// identyczna pierwsza litera
	
	if (w1.length > 0 && w2.length > 0) {
	
	if ( w1.slice(0,1) == w2.slice(0,1) )
		result += 5;
	}
	
    // TODO: usunięcie e jest tańsze
	return result;

  },

}

function isPunctuation(item) {
    return (punctuation.includes(item));
}

var spell = new Spellchecker('spell', false);