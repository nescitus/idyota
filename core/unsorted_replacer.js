function UnsortedReplacer(name) {

  this.one = [];          // forms that should be replaced
  this.two = [];          // their replacements
  this.ln = 0;
  this.name = name;
}

UnsortedReplacer.prototype = {

  Add: function(_in, _out) {

    this.one.push(_in);
    this.two.push(_out);
    this.ln++;
  },
 
  feedArray: function(arr) {
	
    var i = arr.length;
	
    while (i--) {
        if (arr[i].contains(" > ")) {
            var tokens = arr[i].split(" > ");
            this.Add(tokens[0], tokens[1]);
	    } else {
		  alert("Wrong input for dictionary " + this.name + ": " + arr[i]);
		}
	}
		
  },

  getReplacement: function(inp) {
			
    for (i = 0; i < this.ln; i++) {
        if (this.one[i] == inp) {
            return this.two[i];
        }
    }
    
	return inp; // no change	
  },
  
  replacePart: function(word) {

    ln = this.one.length;
   
    while (ln--) {      
		if (word.contains(this.one[ln]) ) {
          word = word.repAll(this.one[ln], this.two[ln]);
          return word;
        }
		
		if (word.contains(capitalizeFirst(this.one[ln])) ) {
            word = word.repAll(capitalizeFirst(this.one[ln]), capitalizeFirst(this.two[ln]));
            return word;
        }  
    }

    return word;
  },
  
    replaceSeries: function(word) {

    ln = this.one.length;
   
    while (ln--) {      
		if (word.contains(this.one[ln]) ) {
          word = word.repAll(this.one[ln], this.two[ln]);
        }
    }

    return word;
  },

}