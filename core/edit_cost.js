    const MAX_CHAR = 500;
    const DELETION_COST = 95;
    const SWAP_COST = 90;
    const DEFAULT_COST = 100;
    const FORGOTTEN_ALT_COST = 94;
    const ADDED_ALT_COST = 94;
    const FONETIC_COST = 90;
    const VOWEL_MODERNIZATION_COST = 89;

function EditCost() {
  this.cost = []; // will be reinitialized as a 2d table
}

EditCost.prototype = {
	
	init: function() {
			
		this.cost = new Array(MAX_CHAR).fill(0).map(() => new Array(MAX_CHAR).fill(0));
			
		for (i = 0; i < MAX_CHAR; i++)
			for (j = 0; j < MAX_CHAR; j++) {
				this.cost[i][j] = 100;
				// TODO: pary samogłosek/spółgłosek tańsze o 1
			}
			

		// writer forgot about alt key

        this.set('l','ł', FORGOTTEN_ALT_COST);
        this.set('e','ę', FORGOTTEN_ALT_COST);
        this.set('a','ą', FORGOTTEN_ALT_COST);
        this.set('z','ż', FORGOTTEN_ALT_COST);
        this.set('x','ź', FORGOTTEN_ALT_COST);
        this.set('o','ó', FORGOTTEN_ALT_COST);
        this.set('n','ń', FORGOTTEN_ALT_COST);

        // writer held alt key for too long

        this.set('ł','l', ADDED_ALT_COST);
        this.set('ę','e', ADDED_ALT_COST);
        this.set('ą','a', ADDED_ALT_COST);
        this.set('ż','z', ADDED_ALT_COST);
        this.set('ź','x', ADDED_ALT_COST);
        this.set('ó','o', ADDED_ALT_COST);
        this.set('ń','n', ADDED_ALT_COST);

        // fonetic errors

        this.set('ó','u', FONETIC_COST);
        this.set('u','ó', FONETIC_COST);
        this.set('s','z', FONETIC_COST);
        this.set('z','s', FONETIC_COST);

        // for old texts

        this.set('j','i', VOWEL_MODERNIZATION_COST);
        this.set('e','y', VOWEL_MODERNIZATION_COST);
        this.set('é','e', 10);
        this.set('é','y', 11);
		this.set('y','i', 11);
		this.set('y','j', 11);
			
	},
	
	set: function(i , j, value) {
		this.cost[i.charCodeAt()][j.charCodeAt()] = value;
	},
	
	get: function(i, j) {
		
		iCode = i.charCodeAt();
		jCode = j.charCodeAt();
		
		if (iCode < 0 || iCode > MAX_CHAR)
           return DEFAULT_COST;		
	   
	    if (jCode < 0 || jCode > MAX_CHAR)
           return DEFAULT_COST;	
		
		return this.cost[iCode][jCode];
	},
	
	calculate: function(firstWord, secondWord) {
	
        firstSize = firstWord.length + 1;
        secondSize = secondWord.length + 1;
				
        matrix = new Array(firstSize).fill(0).map(() => new Array(secondSize).fill(0));
        matrix[0][0] = 0;
		
        for (j = 1; j != secondSize; ++j) {
            matrix[0][j] = matrix[0][j - 1] + DELETION_COST;
        }
		
        firstWord = " " + firstWord;
        secondWord = " " + secondWord;

        let tempCost;
		
        for (i = 1; i != firstSize; ++i) {
            firstChar = firstWord.charAt(i);
			
            matrix[i][0] = matrix[i - 1][0] + DELETION_COST;
            for (j = 1; j != secondSize; ++j) {
				
                secondChar = secondWord.charAt(j);
				
                if (firstChar == secondChar) {
                    matrix[i][j] = matrix[i - 1][j - 1];
                } 
				
				else { 
                    editCost = this.get(firstChar, secondChar);
                    matrix[i][j] = editCost + matrix[i - 1][j - 1];
					
                    if (i != 1 && j != 1
                    && firstChar == secondWord.charAt(j - 1)
                    && firstWord.charAt(i - 1) == secondChar) {
                        tempCost = SWAP_COST + matrix[i - 2][j - 2];
                        if (tempCost < matrix[i][j]) {
                            matrix[i][j] = tempCost;
                        }
                    }

                    tempCost = DELETION_COST + matrix[i - 1][j];
                    if (tempCost < matrix[i][j]) {
                        matrix[i][j] = tempCost;
                    }

                    tempCost = DELETION_COST + matrix[i][j - 1];
                    if (tempCost < matrix[i][j]) {
                        matrix[i][j] = tempCost;
                    }
                }
            }	
        }
		
        return matrix[firstSize - 1][secondSize - 1];
    },
	
	test:function() {
		ble = this.calculate("kon","koń");
		alert("Dystans: kon - koń =" + ble);
		
		ble = this.calculate("hebrata","herbata");
		alert("Dystans: hebrata - herbata =" + ble);
		
		ble = this.calculate("blizkiem","bliskim");
		alert("Dystans: blizkiem - bliskim =" + ble);
		
		ble = this.calculate("pożądanem","pożądałem");
		alert("Dystans: pożądanem - pożądałem =" + ble);
		
		ble = this.calculate("pożądanem","pożądanym");
		alert("Dystans: pożądanem - pożądanym =" + ble);
	}
	
}