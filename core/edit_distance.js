function EditDistance(lrs) {
	this.letters = lrs.split('');
}

EditDistance.prototype = {

  edit1: function(word) {

    var proposals = [];
    var ln = word.length;
    var opt = this.letters.length;
    var pre, post, cur;
   
    for (i = 1; i <= ln; i++) {
        pre = word.slice(0,i);
        post = word.slice(i);
        cur = pre.slice(-1);
        pre = pre.slice(0,-1);
       
        // deletions
       
        if (cur != pre.slice(-1))
           proposals.push(pre+post);
       
        // insertions
       
        for (j = 0; j < opt; j++)
            proposals.push(pre+this.letters[j]+cur+post);    
           
        // replacements
    
        for (j = 0; j < opt; j++) 
            proposals.push(pre+this.letters[j]+post); 
   
        // transpositions
  
        prepre = pre.slice(-1);
        initial = pre.slice(0,-1);
        proposals.push(initial + cur + prepre + post);
   }
   
    // insertions of the last letter
   
    for (j = 0; j < opt; j++)
        proposals.push(word+this.letters[j]);    
   
    return proposals;
  },

  edit1Plain: function(word) {
    
	var proposals = [];
    var ln = word.length;
    var opt = this.letters.length;
    var pre, post, cur;
   
    for (var i = 1; i <= ln; i++) {
	    pre = word.slice(0,i);
	    post = word.slice(i);
	    cur = pre.slice(-1);
	    pre = pre.slice(0,-1);
	   
	    // deletions
	   
	    proposals.push(pre+post);
	   
	    // insertions
	   
	    for (var j = 0; j < opt; j++) {
		    proposals.push(pre+this.letters[j]+cur+post);	
	    }
	   	
	    // replacements
	 
	    for (var j = 0; j < opt; j++) {	
            var last = pre.slice(-1);	   
		    proposals.push(pre+this.letters[j]+post); 
	    }
    }
   
    return proposals;
 },

  edit1Parametrized: function(word, canInsert, canDelete, canReplace, startFrom) {
    var proposals = [];
    var ln = word.length;
    var opt = this.letters.length;
    var pre, post, cur;
   
    for (var i = startFrom+1; i <= ln; i++) {
	    pre = word.slice(0,i);
	    post = word.slice(i);
	    cur = pre.slice(-1);
	    pre = pre.slice(0,-1);
	   
	    // deletions
	   
	    if (canDelete) proposals.push(pre+post);
	   
	    // insertions
	   
	    if (canInsert) {
	        for (var j = 0; j < opt; j++) {
		        proposals.push(pre+this.letters[j]+cur+post);	
	        }
	    }
		
	    // replacements
	
	    if (canReplace) {
	        for (var j = 0; j < opt; j++) {	
                var last = pre.slice(-1);	   
		        proposals.push(pre+this.letters[j]+post);
	        }
	    }
    }
   
    return proposals;
  },

edit2: function(word) {

    var res = [];
    var tmp = edit1Plain(word);
   
    var ln = tmp.length;
    var wordLn = word.length;
   
    for (i = 0; i < ln; i++) {
	    var loc = [];
	    var startFrom = 0;
	   
	    ///////////////////////////////////////////
	    // tmp[i] is within edit distance 1      //
	    // to  the "word" variable. Since tmp[]  //
	    // array contains all such words, it is  //
	    // pointless to change anything before   //
	    // the first modification, because this  //
	    // variant has been already tried while  //
	    // processing words with smaller "i"     //
	    ///////////////////////////////////////////
	   
	    var minLength = ln;
	    if (tmp[i].length < minLength) 
		    minLength = tmp[i].length;
	   
	    for (k = 0; k < minLength; k++) {
	        if (word.charCodeAt(k) != tmp[i].charCodeAt(k)) startFrom = k+1;
	    }
	   
	    ///////////////////////////////////////////////////////
	    // Boolean input variables of edit1Parametrized():   //
	    // canInsert, canDelete, canReplace.                 //
	    //                                                   //
	    // Using them, you may for example decide to avoid   //
	    // generating words created by two replacements      //
	    // or by two delections etc.                         //
	    ///////////////////////////////////////////////////////
	   
	    // after replacement or swapping this.letters
	   
	    if (wordLn == tmp[i].length) {
		    loc = edit1Parametrized(tmp[i], true, true, true, startFrom);
	    }
	   
	    // after insertion

	    if (wordLn < tmp[i].length) {
		    loc = edit1Parametrized(tmp[i], true, true, true, startFrom);
	    }
	   
	    // after deletion
		   
	    if (wordLn > tmp[i].length) {
		    loc = edit1Parametrized(tmp[i], true, true, true, startFrom);
	    }
	   
	    // final verification: filtering words that are too short
	   
	    var ln2 = loc.length;
	    for (var j = 0; j < ln2; j++)
		   if (loc[j].length > 3) 
	 		   res.push(loc[j]);
    }
   
    return res;
  }

}