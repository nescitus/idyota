function changeWording(id) {

    var el = getNamedElement(id);
    var tmp = el.innerHTML;
    el.innerHTML = el.title;
    el.title = tmp;
}

function inDictFormat(val) {
    return '"' + val + '",<br>';	
}

function inQuotes(val) {
    return '"' + val + '"';	
}

function wrapInBold(str) {
    return '<b>' + str + '</b>';
}

function wrapInBrackets(str) {
    return '(' + str + ')';
}

function getProportion(val, div) {
	return Math.floor(val  * 10000 / div);
}

// Displays div content in a separate, unstyled browser window 

function outputDiv(id) {
    var divContents = document.getElementById(id).innerHTML;
    outputString(divContents);
}

// Displays string in a separate, unstyled browser window 

function outputString(string) {
    var a = window.open('', '', 'height=500, width=500');
    a.document.write('<html>');
    a.document.write('<body>');
    a.document.write(string);
    a.document.write('</body></html>');
}

// Helper function: replace all occurences of a substring

String.prototype.repAll = function(_old, _new) {

  // in case string contains chars used in regular expressions:
  _old = _old.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

  var reg = new RegExp(_old, "g");
  return this.replace(reg, _new);   
}

// does a string contain a substring?

String.prototype.contains = function(query) {

  if (this.indexOf(query) != -1) 
	  return true;
  return false;
}

// Find a html element with a given id 
// or complain that it's missing

function getNamedElement(name) {

  var el = document.getElementById(name);
  
  if (el == null) 
	  alert("HTML element " + name + " is missing!");
  
  return el;
}

function spanUnknown(word) {

  return "<span class='red'>"+ word + "</span>";
}

function getLast(arr) {
    return arr[arr.length - 1];
}

function download(filename, text) {

  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}